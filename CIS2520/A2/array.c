#include "array.h"

// The First 80%
/* Malloc's a new struct Array variable, sets up the width, capacity, and nel
 * values to their corresponding values. The array->data variable is memmalloc'ed
 * using the memsys system and is checked to ensure valid pointers before
 * returning the array pointer.
 */
struct Array *newArray( struct memsys *memsys, unsigned int width, unsigned int capacity ) {
    struct Array *array = malloc(sizeof(struct Array));
    if(array == NULL) {
        fprintf(stderr, "[ERROR] - Unable to Allocate struct Array\n");
        exit(0);
    }
    array->width = width;
    array->capacity = capacity;
    array->nel = 0;
    array->data = memmalloc(memsys, (array->width)*(array->capacity));
    if(array->data == MEMNULL) {
        fprintf(stderr, "[ERROR] - Unable to Allocate struct Array\n");
        exit(0);
    }
    return array;
}

/* Checks that the function is fed a valid index position in the Array, then
 * it uses the memsys getval function to get the value at the index, passing
 * it to the dest pointer.
 */
void readItem( struct memsys *memsys, struct Array *array, unsigned int index, void *dest ) {
    if(index >= array->nel) {
        fprintf(stderr, "[ERROR] - Unable to Read item\n");
        exit(0);
    }
    getval(memsys, dest, array->width, array->data+(index*(array->width)));
}

/* Checks that the function is fed a valid index position in the Array, then
 * it uses the memsys setval function to set the value at the index. Finally,
 * the index is checked to see if it is at the end of the array, if so then
 * array->nel is incremented by 1.
 */
void writeItem( struct memsys *memsys, struct Array *array, unsigned int index, void *src ) {
    if(index > array->nel || index >= array->capacity) {
        fprintf(stderr, "[ERROR] - Unable to Write item\n");
        exit(0);
    }
    setval(memsys, src, array->width, array->data+(index*(array->width)));
    if(index == array->nel) {
        array->nel++;
    }
}

/* Shortens the array->nel by 1 unless it is already 0
 */
void contract( struct memsys *memsys, struct Array *array ) {
    if(array->nel <= 0) {
        fprintf(stderr, "[ERROR] - Unable to decrement\n");
    } else {
        array->nel--;
    }
}

/* memfree's the array->data "pointer" and free's the array pointer.
 */
void freeArray( struct memsys *memsys, struct Array *array ) {
    memfree(memsys, array->data);
    free(array);
}

/* Writes an item to the end of the array.
 */
void appendItem( struct memsys *memsys, struct Array *array, void *src ) {
    writeItem(memsys, array, array->nel, src);
}

/* Begins at the end of the Array, coping the last value from the end of the array to
 * the next spot after (increasing the length of the Array by 1). This pattern of
 * copying from one position and pasting it one position later in the Array is followed
 * until the index is reached. The function copies the index to one position later before
 * finally writing the src value into its desired position.
 */
void insertItem( struct memsys *memsys, struct Array *array, unsigned int index, void *src ) {
    int i = 0;
    int ptr = memmalloc(memsys, array->width);
    if(ptr == MEMNULL) {
        fprintf(stderr, "[ERROR] - Unable to Insert item into Array\n");
        exit(0);
    }
    void *memptr = array+ptr;
    for(i=(array->nel); i>index; i--) {
        readItem(memsys, array, i-1, memptr);
        writeItem(memsys, array, i, memptr);
    }
    writeItem(memsys, array, index, src);
    memfree(memsys, ptr);
}
/* Uses insertItem to add an item to the beginning (position 0)
 * of the Array.
 */
void prependItem( struct memsys *memsys, struct Array *array, void *src ) {
    insertItem(memsys, array, 0, src);
}

/* Begins at the index of the item to be deleted, then the item is overwritten with 
 * the next item in the Array. This trend continues through the entire Array, ending
 * with a contraction to remove the final, now unused space.
 */
void deleteItem( struct memsys *memsys, struct Array *array, unsigned int index) {
    int i = 0;
    int ptr = memmalloc(memsys, array->width);
    if(ptr == MEMNULL) {
        fprintf(stderr, "[ERROR] - Unable to Delete item in Array\n");
        exit(0);
    }
    void *memptr = array+ptr;
    for(i=index; i<(array->nel-1); i++) {
        readItem(memsys, array, i+1, memptr);
        writeItem(memsys, array, i, memptr);
    }
    contract(memsys, array);
    memfree(memsys, ptr);
}

// The Last 20%
/* This function goes down the Array (starting at pos. 0) and compares each
 * value in the array to target until it gets a match or reaches the end
 * of the Array values.
 */
int findItem( struct memsys *memsys, struct Array *array, int (*compar)(const void *, const void *), void *target ) {
    int i = 0;
    int ptr = memmalloc(memsys, array->width);
    if(ptr == MEMNULL) {
        fprintf(stderr, "[ERROR] - Unable to Find item in Array\n");
        exit(0);
    }
    void *memptr = array+ptr;
    for(i=0; i<array->nel; i++) {
        if(compar(memptr, target) == 0) {
            memfree(memsys, ptr);
            return i;
        }
    }
    memfree(memsys, ptr);
    return -1;
}

/* Does the same as findItem since I didnt have the time or bother to make the
 * function work the way that is intended but I think that this probably will
 * work just much more inefficiently.
 */
int searchItem( struct memsys *memsys, struct Array *array, int (*compar)(const void *, const void *), void *target ) {
    int i = 0;
    int ptr = memmalloc(memsys, array->width);
    if(ptr == MEMNULL) {
        fprintf(stderr, "[ERROR] - Unable to Find item in Array\n");
        exit(0);
    }
    void *memptr = array+ptr;
    for(i=0; i<array->nel; i++) {
        if(compar(memptr, target) == 0) {
            memfree(memsys, ptr);
            return i;
        }
    }
    memfree(memsys, ptr);
    return -1;
}
