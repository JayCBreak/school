#include "hash.h"

// Functions on HashTableS
// Allocate the memory for a HashTableand initialize the parameters.
struct HashTable *createTable( struct memsys *memsys, unsigned int capacity, unsigned int width, int (*hash)( void *, int), int (*compar)(const void *, const void *) ) {
    struct HashTable *table = malloc(sizeof(struct HashTable));
    int i = 0;
    if(table == NULL) {
        fprintf(stderr, "[ERROR] - Unable to create new HashTable.\n");
        exit(-1);
    }
    table->capacity = capacity;
    table->compar = compar;
    table->data = memmalloc(memsys, capacity*sizeof(int));
    table->hash = hash;
    table->nel = 0;
    table->width = width;
    if(table->data == MEMNULL) {
        fprintf(stderr, "[ERROR] - Unable to create data space for new HashTable.\n");
        exit(-1);
    }
    for(i=(table->data); i<(table->data + capacity*sizeof(int)); i+=sizeof(int)) {
        setval(memsys, MEMNULL, sizeof(int), i);
    }
}
// Add an element to the HashTable with linear probing when a collision occurs.
void addElement( struct memsys *memsys, struct HashTable *table, int addr ) {
    if(table->nel == table->capacity) {
        fprintf(stderr, "[ERROR] - Unable to addElement, max capacity.\n");
        exit(-1);
    }
    int data;
    int tempData = memmalloc(memsys, table->width);
    int index, i;
    if(data == NULL || tempData == NULL) {
        fprintf(stderr, "[ERROR] - Unable to addElement, max capacity.\n");
        exit(-1);
    }
    getval(memsys, data, table->width, addr);
/* 5Head stuff here ngl. To use the index first you must find the beginning of the
 * Array (table->data), then calculate the hash of the data to insert to find out
 * where it will go in the Array. Add the table->data to hash*sizeof(int) to get
 * the address of the data at Array[index]. Just add sizeof(int) to get subsequent
 * items in the Array.
 */
    index = table->hash(data, table->capacity);
    index *= sizeof(int);
    index += table->data;
    getval(memsys, tempData, table->width, index);
    while(tempData != MEMNULL) {
/* ORDERING HERE MIGHT CAUSE BUG
 * might not check the last item in the data array.
 */ 
        index += sizeof(int);
        if((index-(table->data))/sizeof(int) >= table->capacity) {
            index = table->data;
        }
        getval(memsys, tempData, table->width, index);
        i++;
    }
    setval(memsys, &addr, table->width, index);
    free(data);
    free(tempData);
}
// Find an element in the HashTable, return its memsys address.
int getElement( struct memsys *memsys, struct HashTable *table, void *key ) {
    void *data = malloc(table->width);
    if(data == NULL) {
        fprintf(stderr, "[ERROR] - Unable to getElement.\n");
        exit(-1);
    }
    int addr = table->hash(key, table->capacity);
    addr *= sizeof(int);
    addr += table->data;
    getval(memsys, data, table->width, addr);
    if(data == MEMNULL) {
        return MEMNULL;
    } else {
        
    }

}
// Free the Table.
void freeTable( struct memsys *memsys, struct HashTable *table ) {

}

// Last 20%
// Compute the hash accuracy of the contents of the HashTable.
int hashAccuracy( struct memsys *memsys, struct HashTable *table ) {

}