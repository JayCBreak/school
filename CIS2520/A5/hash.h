#include "memsys.h"
#ifndef STDIO_H
#include <stdio.h>
#endif
#ifndef STDLIB_H
#include <stdlib.h>
#endif
#ifndef STRING_H
#include <string.h> // For size_t
#endif
#ifndef HASH_H
#define HASH_H

struct HashTable
{
    unsigned int capacity;
    unsigned int nel;
    unsigned int width;
    int data;
    int (*hash)( void *, int);
    int (*compar)(const void *, const void *);
};
// Functions on HashTableS
struct HashTable *createTable( struct memsys *memsys, unsigned int capacity, unsigned int width, int (*hash)( void *, int), int (*compar)(const void *, const void *) );
void addElement( struct memsys *memsys, struct HashTable *table, int addr );
int getElement( struct memsys *memsys, struct HashTable *table, void *key );
void freeTable( struct memsys *memsys, struct HashTable *table );
// Last 20%
int hashAccuracy( struct memsys *memsys, struct HashTable *table );

#endif
