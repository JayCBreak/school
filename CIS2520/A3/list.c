#include "list.h"

// Functions on NodeS
void push( struct memsys *memsys, int *node_ptr, void *src, size_t width ) {
    int newAddr = 0;
    newAddr = memmalloc(memsys, width);
    if(newAddr == MEMNULL) {
        fprintf(stderr, "Unable to push value.\n");
        exit(-1);
    }
    setval(memsys, src, width, newAddr);
    struct Node *newNode = malloc(sizeof(struct Node));
    if(newNode == NULL) {
        fprintf(stderr, "Unable to push value.\n");
        exit(-1);
    }
    newNode->data = newAddr;
    newNode->next = *node_ptr;
    *node_ptr = memmalloc(memsys, sizeof(struct Node));
    if(*node_ptr == MEMNULL) {
        fprintf(stderr, "Unable to push value.\n");
        exit(-1);
    }
    setval(memsys, newNode, sizeof(struct Node), *node_ptr);
    free(newNode);
}

void insert( struct memsys *memsys, int *node_ptr, void *src, size_t width ) {
    struct Node *adr = malloc(sizeof(struct Node));
    if(adr == NULL) {
        fprintf(stderr, "Unable to insert value.\n");
        exit(-1);
    }
    getval(memsys, adr, sizeof(struct Node), *node_ptr);
    int insertData = memmalloc(memsys, width);
    if(insertData == MEMNULL) {
        fprintf(stderr, "Unable to insert value.\n");
        exit(-1);
    }
    setval(memsys, src, width, insertData);
    struct Node *newNode = malloc(sizeof(struct Node));
    if(newNode == NULL) {
        fprintf(stderr, "Unable to insert value.\n");
        exit(-1);
    }
    newNode->data = insertData;
    newNode->next = adr->next;
    int insertAdr = memmalloc(memsys, sizeof(struct Node));
    if(insertAdr == MEMNULL) {
        fprintf(stderr, "Unable to insert value.\n");
        exit(-1);
    }
    setval(memsys, newNode, sizeof(struct Node), insertAdr);
    adr->next = insertAdr;
    setval(memsys, adr, sizeof(struct Node), *node_ptr);
    free(adr);
    free(newNode);
}

void delete( struct memsys *memsys, int *node_ptr ) {
    struct Node *firstNode = malloc(sizeof(struct Node));
    struct Node *tempNextNode = malloc(sizeof(struct Node));
    if(firstNode == NULL || tempNextNode == NULL) {
        fprintf(stderr, "Unable to delete value.\n");
        exit(-1);
    }
    getval(memsys, firstNode, sizeof(struct Node), *node_ptr);
    getval(memsys, tempNextNode, sizeof(struct Node), firstNode->next);
    memfree(memsys, firstNode->next);
    memfree(memsys, tempNextNode->data);
    firstNode->next = tempNextNode->next;
    setval(memsys, firstNode, sizeof(struct Node), *node_ptr);
    free(firstNode);
    free(tempNextNode);
}

void readHead( struct memsys *memsys, int *node_ptr, void *dest, unsigned int width ) {
    struct Node *node = malloc(sizeof(struct Node));
    if(*node_ptr == MEMNULL) {
        fprintf(stderr, "Unable to readHead from empty node.\n");
        exit(-1);
    }
    getval(memsys, node, sizeof(struct Node), *node_ptr);
    getval(memsys, dest, width, node->data);
    free(node);
}

void pop( struct memsys *memsys, int *node_ptr ) {
    struct Node *node = malloc(sizeof(struct Node));
    if(node == NULL || *node_ptr == MEMNULL) {
        fprintf(stderr, "Unable to pop value.\n");
        exit(-1);
    }
    getval(memsys, node, sizeof(struct Node), *node_ptr);
    memfree(memsys, *node_ptr);
    *node_ptr = node->next;
    memfree(memsys, node->data);
    free(node);
}

int next( struct memsys *memsys, int *node_ptr ) {
    struct Node *node = malloc(sizeof(struct Node));
    int nextNode;
    if(node == NULL || *node_ptr == MEMNULL) {
        fprintf(stderr, "Unable to readHead from empty node.\n");
        exit(-1);
    }
    getval(memsys, node, sizeof(struct Node), *node_ptr);
    nextNode = node->next;
    free(node);
    return nextNode;
}

int isNull( struct memsys *memsys, int *node_ptr ) {
    if(*node_ptr == MEMNULL) {
        return 1;
    } else {
        return 0;
    }
}

// Functions on ListS
struct List *newList( struct memsys *memsys, unsigned int width ) {
    struct List *list = malloc(sizeof(struct List));
    if(list == NULL) {
        fprintf(stderr, "Unable to create a new list.\n");
        exit(-1);
    }
    list->width = width;
    list->head = MEMNULL;
    return list;
}

void freeList( struct memsys *memsys, struct List *list ) {
    while(list->head != MEMNULL) {
        pop(memsys, &list->head);
    }
    free(list);
}

int isEmpty( struct memsys *memsys, struct List *list ) {
    if(isNull(memsys, &list->head)) {
        return 1;
    } else {
        return 0;
    }
}

void readItem( struct memsys *memsys, struct List *list, unsigned int index, void *dest ) {
    int i = 0;
    int ptr = list->head;
    for(i=0; i<index; i++) {
        ptr = next(memsys, &ptr);
    }
    readHead(memsys, &ptr, dest, list->width);
}

void appendItem( struct memsys *memsys, struct List *list, void *src ) {
    int i = 0;
    int ptr = list->head;
    int prevPtr = 0;
    for(i=0; !isNull(memsys, &ptr); i++) {
        prevPtr = ptr;
        ptr = next(memsys, &ptr);
    }
    insert(memsys, &prevPtr, src, list->width);
}

void insertItem( struct memsys *memsys, struct List *list, unsigned int index, void *src ) {
    int i = 0;
    int ptr = list->head;
    if(index == 0) {
        push(memsys, &list->head, src, list->width);
    } else {
        for(i=0; i<index; i++) {
            ptr = next(memsys, &ptr);
        }
        insert(memsys, &ptr, src, list->width);
    }
}

void prependItem( struct memsys *memsys, struct List *list, void *src ) {
    insertItem(memsys, list, 0, src);
}

void deleteItem( struct memsys *memsys, struct List *list, unsigned int index ) {
    int i = 0;
    int ptr = list->head;
    if(index == 0) {
        pop(memsys, &ptr);
    } else {
        for(i=0; i<index; i++) {
            ptr = next(memsys, &ptr);
        }
        delete(memsys, &ptr);
    }
}


// Last 20%
int findItem( struct memsys *memsys, struct List *list, int(*compar)(const void *, const void *), void *target ) {
    exit(0);
    return 0;
}
