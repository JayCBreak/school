#include "memsys.h"
#ifndef STDIO_H
#define STDIO_H
#include <stdio.h>
#endif
#ifndef STDLIB_H
#define STDLIB_H
#include <stdlib.h>
#endif
#ifndef ARRAY_H
#define ARRAY_H

// The First 80%
struct Array
{
 unsigned int width;
 unsigned int nel;
 unsigned int capacity;
 int data;
};
struct Array *newArray( struct memsys *memsys, unsigned int width, unsigned int capacity );
void readItem( struct memsys *memsys, struct Array *array, unsigned int index, void *dest );
void writeItem( struct memsys *memsys, struct Array *array, unsigned int index, void *src );
void contract( struct memsys *memsys, struct Array *array );
void freeArray( struct memsys *memsys, struct Array *array );
void appendItem( struct memsys *memsys, struct Array *array, void *src );
void insertItem( struct memsys *memsys, struct Array *array, unsigned int index, void *src );
void prependItem( struct memsys *memsys, struct Array *array, void *src );
void deleteItem( struct memsys *memsys, struct Array *array, unsigned int index);

// The Last 20%
int findItem( struct memsys *memsys, struct Array *array, int (*compar)(const void *, const void *), void *target );
int searchItem( struct memsys *memsys, struct Array *array, int (*compar)(const void *, const void *), void *target );
#endif
