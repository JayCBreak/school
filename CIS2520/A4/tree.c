#include "tree.h"

// Functions on NodeS
// Create a node; store its address.
void attachNode( struct memsys *memsys, int *node_ptr, void *src, unsigned int width ) {
    struct Node *node = malloc(sizeof(struct Node));
    *node_ptr = memmalloc(memsys, sizeof(struct Node));
    node->data = memmalloc(memsys, width);
    if(node == NULL || *node_ptr == MEMNULL || node->data == MEMNULL) {
        fprintf(stderr, "[ERROR] - Unable to attach node.\n");
        exit(-1);
    }
    setval(memsys, src, width, node->data);
    node->gte = MEMNULL;
    node->lt = MEMNULL;
    setval(memsys, node, sizeof(struct Node), *node_ptr);
    free(node);
}
// Attach a child node to the parent.
void attachChild( struct memsys *memsys, int *node_ptr, void *src, unsigned int width, int direction ) {
    struct Node *node = malloc(sizeof(struct Node));
    int newNode = MEMNULL;
    if(node == NULL) {
        fprintf(stderr, "[ERROR] - Unable to attach child.\n");
        exit(-1);
    }
    getval(memsys, node, sizeof(struct Node), *node_ptr);
    attachNode(memsys, &newNode, src, width);
    if(direction < 0) {
        node->lt = newNode;
    } else {
        node->gte = newNode;
    }
    setval(memsys, node, sizeof(struct Node), *node_ptr);
    free(node);
}
// Compare data in a variable to data in a node.
int comparNode( struct memsys *memsys, int *node_ptr, int (*compar)(const void *, const void *), void *target, unsigned int width) {
    struct Node *node = malloc(sizeof(struct Node));
    void *nodeData = malloc(width);
    int result;
    if(node == NULL || nodeData == NULL) {
        fprintf(stderr, "[ERROR] - Unable to compare node.\n");
        exit(-1);
    }
    getval(memsys, node, sizeof(struct Node), *node_ptr);
    getval(memsys, nodeData, width, node->data);
    result = compar(target, nodeData);
    free(node);
    free(nodeData);
    return result;
}
// Determine the next node in the tree to visit.
int next( struct memsys *memsys, int *node_ptr, int direction ) {
    struct Node *node = malloc(sizeof(struct Node));
    int result;
    if(node == NULL) {
        fprintf(stderr, "[ERROR] - Unable to get the next node.\n");
        exit(-1);
    }
    getval(memsys, node, sizeof(struct Node), *node_ptr);
    if(direction < 0) {
        result = node->lt;
    } else {
        result = node->gte;
    }
    free(node);
    return result;
}
// Copy data from a node in the tree into dest.
void readNode( struct memsys *memsys, int *node_ptr, void *dest, unsigned int width) {
    struct Node *node = malloc(sizeof(struct Node));
    if(node == NULL || *node_ptr == MEMNULL) {
        fprintf(stderr, "[ERROR] - Unable to read empty node.\n");
        exit(-1);
    }
    getval(memsys, node, sizeof(struct Node), *node_ptr);
    getval(memsys, dest, width, node->data);
    free(node);
}
// Remove a leaf node from the tree.
void detachNode( struct memsys *memsys, int *node_ptr ) {
    struct Node *node = malloc(sizeof(struct Node));
    if(node == NULL || *node_ptr == MEMNULL) {
        fprintf(stderr, "[ERROR] - Unable to detach node.\n");
        exit(-1);
    }
    getval(memsys, node, sizeof(struct Node), *node_ptr);
    memfree(memsys, *node_ptr);
    memfree(memsys, node->data);
    *node_ptr = MEMNULL;
    free(node);
}
// Free all the nodes including and below node_ptr.
void freeNodes( struct memsys *memsys, int *node_ptr ) {
    int lt, gte;
    lt = next(memsys, node_ptr, -1);
    gte = next(memsys, node_ptr, 1);
    if(lt != MEMNULL) {
        freeNodes(memsys, &(lt));
    }
    if(gte != MEMNULL) {
        freeNodes(memsys, &(gte));
    }
    detachNode(memsys, node_ptr);
}

// Functions on TreeS
// Create a new tree.
struct Tree *newTree( struct memsys *memsys, unsigned int width ) {
    struct Tree *tree = malloc(sizeof(struct Tree));
    if(tree == NULL) {
        fprintf(stderr, "[ERROR] - Unable to create new tree.\n");
        exit(-1);
    }
    tree->width = width;
    tree->root = MEMNULL;
    return tree;
}
// Free the tree.
void freeTree( struct memsys *memsys, struct Tree *tree) {
    freeNodes(memsys, &(tree->root));
    free(tree);
}
// Add an item to the tree at the appropriate spot.
void addItem( struct memsys *memsys, struct Tree *tree, int (*compar)(const void *, const void *), void *src ) {
    int node, prevNode, cmpVal;
    node = tree->root;
    prevNode = node;
    if(tree->root == MEMNULL) {
        attachNode(memsys, &(tree->root), src, tree->width);
    } else {
        while(node != MEMNULL) {
            prevNode = node;
            cmpVal = comparNode(memsys, &node, compar, src, tree->width);
            node = next(memsys, &node, cmpVal);
        }
        attachChild(memsys, &prevNode, src, tree->width, cmpVal);
    }
}

// Last 20%
// Search for a Node
int searchItem( struct memsys *memsys, struct Tree *tree, int(*compar)(const void *, const void *), void *target) {
    return 0;
}
