#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "a1.h"
#include "get_bits.h"

#define ALPHABET_LENGTH 26

unsigned char checksum( char *string ) {
    int i = 0;
    unsigned char checksum = 0;
    for(i=0; string[i] != '\0'; i++) {
        if (tolower(string[i]) >= 'a' && tolower(string[i]) <= 'z') {
            checksum += tolower(string[i])-'a';
        }
    }
    checksum = checksum%ALPHABET_LENGTH;
    return checksum;
}

void caesar( char *string, int rshift ) {
    int i = 0;
    for(i=0; string[i] != '\0'; i++) {
        if(isupper(string[i])) {
            string[i] += rshift;
            if(string[i] > 'Z') {
                string[i] -= ALPHABET_LENGTH;
            } else if (string[i] < 'A') {
                string[i] += ALPHABET_LENGTH;
            }
        }
    }
}

void char2bits( char c, unsigned char bits[8] ) {
    int i = 0;
    for(i=0; i<=7; i++) {
        bits[i] = get_bits8(i, i+1, &c);
    }
}

void bits2str( int bitno, unsigned char *bits, char *bitstr ) {
    int i = 0;
    for(i=0; i<bitno; i++) {
        bitstr[i] = bits[i]+'0';
    }
    bitstr[i] = '\0';
}

void ushort2bits( unsigned short s, unsigned char bits[16] ) {
    int i = 0;
    for(i=0; i<=15; i++) {
        bits[i] = get_bits16(i, i+1, &s);
    }
}

void short2bits(short s, unsigned char bits[16] ) {
    int i = 0;
    for(i=0; i<=15; i++) {
        bits[i] = get_bits16(i, i+1, &s);
    }
}

short bits2short( char *bits ) {
    int i = 0;
    short total = 0;
    if(bits[i] == '1') {
        total += shortPow(-2, 15);
    }
    for(i=1; i<strlen(bits); i++) {
        if(bits[i] == '1') {
            total += shortPow(2, 15-i);
        }
    }
    return total;
}

short shortPow(int base, int exp) {
    short total = 1;
    while(exp != 0) {
        total *= base;
        --exp;
    }
    return total;
}

// The Last 10%
void spff( char *sign, char *exponent, char *significand, float *src ) {
    unsigned char charBits = 0;
    // Sign
    charBits = (unsigned char)get_bits32(0, 1, src);
    bits2str(1, &charBits, sign);
    // Exponent
    /* charBits = (unsigned char)get_bits32(1, 9, src);
    bits2str(8, &charBits, exponent);
    // Significand
    
    for(i=0; i<=22; i++) {
        significand[i] = get_bits32(i+9, i+10, src);
    }
    significand[i] = '\0'; */
}
