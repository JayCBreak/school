import java.io.*;
import java.util.Scanner;

public class Lab1 {
    public static void main(String[] args) {
        int sentenceCount = 0;
        int selectionNum = 0;
        String[] sentences = new String[10];
        Scanner input = new Scanner(System.in);
        String search = new String();
        while(true) {
            System.out.println("********************************");
            System.out.println("|             MENU             |");
            System.out.println("|                              |");
            System.out.println("|1) Enter a Sentence.          |");
            System.out.println("|2) Print Sentences.           |");
            System.out.println("|3) Print Sentences in Reverse.|");
            System.out.println("|4) Print # of Sentences.      |");
            System.out.println("|5) Print # of Characters.     |");
            System.out.println("|6) Print # of Vowels.         |");
            System.out.println("|7) Sensitive Word Search.     |");
            System.out.println("|8) Insensitive Word Search.   |");
            System.out.println("|9) End Program.               |");
            System.out.println("********************************");
            System.out.print("Enter Selection: ");
            try {
                selectionNum = input.nextInt();
            } catch (Exception InputMismatchException) {
                System.err.println("[ERROR] - Please only input Integer values.");
                input.close();
                System.exit(0);
            }
            input.skip("\n");
            if(selectionNum == 1) {
                if(sentenceCount >= 10) {
                    System.err.println("[ERROR] - Maximum Sentences Reached (10)");
                } else {
                    System.out.println("Please Enter your Sentence: ");
                    sentences[sentenceCount] = input.nextLine();
                    sentenceCount++;
                }
            } else if(selectionNum == 2) {
                for(int i=0; i<sentenceCount; i++) {
                    System.out.println(i+1 + ") " + sentences[i]);
                }
            } else if(selectionNum == 3) {
                for(int i=sentenceCount; i>0; i--) {
                    System.out.println(i + ") " + sentences[i-1]);
                }
            } else if(selectionNum == 4) {
                System.out.println("The Number of Sentences is: " + sentenceCount);
            } else if(selectionNum == 5) {
                int charCount = 0;
                for(int i=0; i<sentenceCount; i++) {
                    charCount += sentences[i].length();
                }
                System.out.println("The Number of Characters is: " + charCount);
            } else if(selectionNum == 6) {
                int vowelCount = 0;
                for(int i=0; i<sentenceCount; i++) {
                    for(int j=0; j<sentences[i].length(); j++)
                    if(sentences[i].toLowerCase().charAt(j) == 'a') {
                        vowelCount += 1;
                    } else if(sentences[i].toLowerCase().charAt(j) == 'e') {
                        vowelCount += 1;
                    } else if(sentences[i].toLowerCase().charAt(j) == 'i') {
                        vowelCount += 1;
                    } else if(sentences[i].toLowerCase().charAt(j) == 'o') {
                        vowelCount += 1;
                    } else if(sentences[i].toLowerCase().charAt(j) == 'u') {
                        vowelCount += 1;
                    }
                }
                System.out.println("The Number of Vowels is: " + vowelCount);
            } else if(selectionNum == 7) {
                boolean containsWord = false;
                System.out.print("Input the word to search: ");
                search = input.next();
                for(int i=0; i<sentenceCount; i++) {
                    for(int j=0; j<sentences[i].split("\\W+").length ; j++) {
                        if(sentences[i].split("\\W+")[j].equals(search)) {
                            containsWord = true;
                        }
                    }
                    if(containsWord) {
                        System.out.println(sentences[i]);
                        containsWord = false;
                    }
                }
            } else if(selectionNum == 8) {
                boolean containsWord = false;
                System.out.print("Input the word to search: ");
                search = input.next();
                for(int i=0; i<sentenceCount; i++) {
                    for(int j=0; j<sentences[i].split("\\W+").length ; j++) {
                        if(sentences[i].split("\\W+")[j].equalsIgnoreCase(search)) {
                            containsWord = true;
                        }
                    }
                    if(containsWord) {
                        System.out.println(sentences[i]);
                        containsWord = false;
                    }
                }
            } else if(selectionNum == 9) {
                input.close();
                System.out.println("Thanks! Goodbye.");
                System.exit(0);
            } else {
                System.err.println("[ERROR] - Invalid Menu Selection: " + selectionNum);
            }
            selectionNum = 0;
        }
    }
}